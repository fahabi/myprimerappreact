import React from 'react';

import api from "../api";
import PageLoading from "../components/PageLoading";
import PageError from "../components/PageError";
import BadgeDetails from "./BadgeDetails";

//este COntainer se va a encargar de la logica de negocio
class BadgeDetailsContainer extends React.Component{

    state = {
        loading: true,
        error: null,
        data: undefined,
        modalIsOpen: false,
    };

    componentDidMount() {
        this.fetchData()
    }

    fetchData = async () => {
        this.setState({
            loading: true,
            error: null
        });

        try {
            const data = await api.badges.read(
                this.props.match.params.badgeId
            );
            this.setState({
                loading:false,
                data: data,
            })
        } catch (error) {
            this.setState({
                loading:false,
                error: error,
            })
        }
    };

    handleOpenModal = e => {
        this.setState({
            modalIsOpen: true,
        });
    };

    handleCloseModal = e => {
        this.setState({
            modalIsOpen: false,
        });
    };

    handleDeleteBadge = async e => {
        this.setState({
            loading: true,
            error: null,
        });

        try {
            await api.badges.remove(
                this.props.match.params.badgeId
            );
            this.props.history.push("/badges");

        } catch (error) {
            this.setState({
                loading: false,
                error: error,
            })
        }
    };

    render() {
        if (this.state.loading) return <PageLoading/>;
        if (this.state.error) return <PageError error={this.state.error}/>;

        //guardo en una constante para solo llamar a *badge*. y la propiedad
        //const badge = this.state.data;

        return (
            <BadgeDetails
                onCloseModal={this.handleCloseModal}
                onOpenModal={this.handleOpenModal}
                onDeleteBadge={this.handleDeleteBadge}
                modalIsOpen={this.state.modalIsOpen}
                badge={this.state.data}
            />
        );
    }
}

export default BadgeDetailsContainer;