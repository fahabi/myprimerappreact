import React from 'react';

import BadgeForm from '../components/BadgeForm';
import Badge from "../components/Badge";
import api from "../api";
import PageLoading from "../components/PageLoading";

class BadgeNew extends React.Component {

    state = {
        loading: false,
        error: null,
        form: {
            nombre: '',
            apellido: '',
            edad: '',
            email: ''
        }
    };

    handleChange = e => {
        this.setState({
            form: {
                //[e.target.name]: e.target.value,
                ...this.state.form, // mantenemos todos los valores cargados anteriormente
                [e.target.name]: e.target.value,
            },
        });
    };

    handleSubmit = async e => {
        e.preventDefault();
        this.setState({
            loading: true,
            error: null,
        });

        try {
            await api.badges.create(this.state.form);
            this.setState({loading: false });
            this.props.history.push('/badges');
        } catch (error) {
            this.setState({
                loading: false,
                error: error,
            })
        }
    };

    render() {
        
        if (this.state.loading){
            return <PageLoading/>
        }

        return (
            <React.Fragment>
                <div className="col-md-12">
                    <div className="row">
                        <div className="offset-1 col-md-5">
                            <Badge
                                nombre={this.state.form.nombre || 'Primer Nombre'}
                                apellido={this.state.form.apellido || 'Apellido'}
                                email={this.state.form.email || 'Email'}
                            />
                        </div>
                        <div className="col-md-5">
                            <h1>Formulario New</h1>
                            <BadgeForm
                                onChange={this.handleChange}
                                onSubmit={this.handleSubmit}
                                formValues={this.state.form}
                                error={this.state.error}
                            />
                        </div>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

export default BadgeNew;