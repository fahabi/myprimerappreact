import React from "react"
import confLogo from "../images/logo.svg";
import {Link} from "react-router-dom";
import DeleteBadgeModal from "../components/DeleteBadgeModal";

function useIncreseCount(max) {
    const [ count, setCount ] = React.useState(0);

    if (count > max) {
        setCount(0)
    }

    return [ count, setCount ];
}

function BadgeDetails (props){

    //const [ count, setCount ] = React.useState(0);

    const [ count, setCount ] = useIncreseCount(4);

    const badge = props.badge;

    return(
        <div>
            <div className="container">
                <div className="row">
                    <div className="col-6">
                        <img src={confLogo} alt="Logo de la conferencia"/>
                    </div>
                    <div className="col-6">
                        <h1>
                            {badge.nombre} {badge.apellido}
                        </h1>
                    </div>
                </div>
            </div>

            <div className="container">
                <div className="row">
                    <div className="col-6">
                        {badge.email}
                    </div>
                    <div className="col-6">
                        <h2> Actions </h2>
                        <div>
                            <button className="btn btn-primary mr-4" onClick={ () => {
                                setCount( count + 1 );
                            } }>
                                Contador : {count}
                            </button>
                            <Link className="btn btn-warning mb-4" to={`/badges/${badge.id}/edit`}>
                                Editar
                            </Link>
                        </div>

                        <div>
                            <button className="btn btn-danger" onClick={props.onOpenModal}>
                                Borrar
                            </button>
                            <DeleteBadgeModal
                                isOpen={props.modalIsOpen}
                                onClose={props.onCloseModal}
                                onDeleteBadge={props.onDeleteBadge}
                            />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default BadgeDetails;