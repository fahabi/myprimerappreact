import React from "react"
import BadgesList from "../components/BadgesList";
import PageLoading from "../components/PageLoading";
import PageError from "../components/PageError";
import {Link} from "react-router-dom";
import api from "../api";
import MiniLoader from "../components/MiniLoader";

class Badges extends React.Component{

    constructor(props) {
        // constructor de la class
        super(props);
        console.log('1. constructor()');

        this.state = {
            loading: true,
            error: null,
            data: undefined,
        };
    }

    componentDidMount() {
        // quiero q cada 3 segundos se carge el data con el contenido (puede ser una API) -> sabemos q los datos estan listos
        console.log('3. componentDidMount()');

        this.fetchData();

        this.intervalId = setInterval(this.fetchData, 5000)

        //this.timeoutId = setTimeout(() =>{
        //    this.setState({
        //        data: dataJson.data,
        //    })
        //},3000);
    }

    fetchData = async () => {
        this.setState({ loading: true, error: null });

        //inicio llamada a la API
        try {
            const data = await api.badges.list();

            this.setState({
                loading: false,
                data: data,
            });
        } catch (error) {
            this.setState({
                loading: false,
                error: error,
            });
        }
    };

    componentDidUpdate(prevProps, prevState) {
        // cuando hago un update de alguna info como el data
        console.log('5. componentDidUpdate()');

        console.log({
            prevProps: prevProps,
            prevState: prevState
        });

        console.log({
            Props: this.props,
            State: this.state
        });
    }

    componentWillUnmount() {
        //cuando cambio de pagina (salgo del DOM)
        console.log('6. componentDidUpdate()');

        // si hay algun update esperando lo limpio
        clearTimeout(this.timeoutId);
        clearInterval(this.intervalId);
    }

    render() {
        console.log('2/4. render()');

        if (this.state.loading === true && !this.state.data){
            return <PageLoading />;
        }

        if (this.state.error) {
            return <PageError error={this.state.error} />;
        }

        return (
            <div>
                <p className="text-danger text-center alert-danger col-3">
                    Prueba Badges
                </p>

                <div>
                    <Link to="/badges/new" className="btn btn-primary"> Boton New Badge </Link>
                </div>

                <div className="container-fluid col-6 text-center">
                    <BadgesList badges={this.state.data}/>

                    {this.state.loading && <MiniLoader/>}
                </div>
            </div>
        );
    }

}

export default Badges;