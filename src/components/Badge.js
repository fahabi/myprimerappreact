import React from 'react';
import "../styles/Badge.css";
import confLogo from '../images/logo.svg';
import Gravatar from "./Gravatar";

class Badge extends React.Component {

    render() {

        return(
            <div className="Badge col-md-6">
                <div>
                    <Gravatar src={confLogo} alt="Logo" width="50px" email={this.props.email}></Gravatar>
                </div>

                <div>
                    <h1> {this.props.nombre} <br/> {this.props.apellido} </h1>
                </div>

                <div>
                    <p> Frontend </p>
                    <p> @tuPrima </p>
                </div>

                <div>
                    #footer
                </div>
            </div>
        );
    }
}

export default Badge;