import React from 'react';

class BadgeForm extends React.Component {

    state = {};

/*
    handleChange = (e) => {
        //console.log(e); //recibe el evento
        //console.log({value: e.target.value}); //accedo al valor;
        //console.log({value: e.target.name});
        //console.log({value: e.target.id});
        //console.log({value: e.target.className});
        //console.log({value: e.target.type});

        this.setState({
            [e.target.name]: e.target.value,
        });
    };
*/
    handleClick = (e) => {
        //console.log({value: e.target.value});
    };

    //handleSubmit = (e) => {
    //    //e.preventDefault();
    //    console.log(this.state);
    //};

    render() {
        return (
            <div>
                <form onSubmit={this.props.onSubmit}>
                    <div className="form-group">
                        <label htmlFor="nombre"> Nombre: </label>
                        <input onChange={this.props.onChange}
                               className="form-control"
                               type="text"
                               name="nombre"
                               value={this.props.formValues.nombre}
                        />
                    </div>
                    <div className="form-group">
                        <label htmlFor="apellido"> Apellido: </label>
                        <input onChange={this.props.onChange}
                               className="form-control"
                               type="text"
                               name="apellido"
                               value={this.props.formValues.apellido}
                        />
                    </div>
                    <div className="form-group">
                        <label htmlFor="email"> Email: </label>
                        <input onChange={this.props.onChange}
                               className="form-control"
                               type="email"
                               name="email"
                               value={this.props.formValues.email}
                        />
                    </div>
                    <div className="form-group">
                        <label htmlFor="edad"> Edad: </label>
                        <input onChange={this.props.onChange}
                               className="form-control"
                               type="number"
                               name="edad"
                               value={this.props.formValues.edad}
                        />
                    </div>

                    <button onClick={this.handleClick} className="btn btn-primary"> Guardar </button>

                    {this.props.error && <p className="text-danger"> {this.props.error.message} </p>}
                </form>
            </div>
        );
    }
}

export default BadgeForm;