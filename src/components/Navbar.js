import React from 'react';
import {Link} from "react-router-dom";

class Navbar extends React.Component {

    render() {
        return (
            <div className="container-fluid">
                <Link to="/">
                    <span className="font-weight-light"> ## React ## </span>
                    <span className="font-weight-bold"> Prueba (Nabvar) </span>
                </Link>
            </div>
        );
    }
}

export default Navbar;