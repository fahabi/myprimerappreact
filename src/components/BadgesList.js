import React from "react"
import {Link} from "react-router-dom";

import './styles/BadgesList.css';
import Gravatar from "./Gravatar";

class BadgesListItem extends React.Component {
    render() {
        return (
            <div className="BadgesListItem">
                <Gravatar
                    className="BadgesListItem__avatar"
                    email={this.props.badge.email}
                />

                <div>
                    <strong>
                        {this.props.badge.nombre} {this.props.badge.apellido}
                    </strong>
                    <br />@hola
                    <br />
                </div>
            </div>
        );
    }
}

function useSearchBadges(badges) {

    const [ query, setQuery ] = React.useState("");

    const [ filteredBadges, setFilteredBadges] = React.useState(badges);

    React.useMemo(

        () => {
            const result = badges.filter(badge => {
                return `${badge.nombre} ${badge.apellido}`
                    .toLowerCase()
                    .includes(query.toLowerCase());
            });

            setFilteredBadges(result);

        }, [ badges, query ]
    );

    return { query, setQuery, filteredBadges };
}

function BadgesList (props){

    const badges = props.badges;

    const { query, setQuery, filteredBadges } = useSearchBadges( badges );

    if (filteredBadges.length === 0) {
        return (
            <React.Fragment>
                <div className="form-group">
                    <label>Filtrar:</label>
                    <input type="text" className="form-control" onK
                           value={query}
                           onChange={(event => {
                               setQuery(event.target.value)
                           })}
                    />
                </div>
                <div className="alert-danger">
                    No se obtuvieron resultados
                    <Link className="btn btn-danger text-left" to="/badges/new"> Create new badge </Link>
                </div>
            </React.Fragment>
        );
    }

    return (
        <div className="BadgesList">

            <div className="form-group">
                <label>Filtrar:</label>
                <input type="text" className="form-control"
                       value={query}
                       onChange={(event => {
                           setQuery(event.target.value)
                       })}
                />
            </div>

            <ul className="list-unstyled">
                {filteredBadges.map(badge => {
                    return (
                        <li key={badge.id}>
                            <Link className="text-reset text-decoration-none" to={`/badges/${badge.id}`}>
                                <BadgesListItem badge={badge} />
                            </Link>
                        </li>
                    );
                })}
            </ul>
        </div>
    );
}

export default BadgesList;