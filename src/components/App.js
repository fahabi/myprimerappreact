import React from "react";
import {BrowserRouter, Switch, Route} from "react-router-dom";

import Layout from "../components/Layout";
import Badges from "../pages/Badges";
import BadgeDetailsContainer from "../pages/BadgeDetailsContainer";
import BadgeNew from "../pages/BadgeNew";
import BadgeEdit from "../pages/BadgeEdit";
import NotFound from "../components/NotFound";

// es como si fuera el metodo render pero en una funcion
function App() {

    return (
        <BrowserRouter>
            <Layout>
                <Switch>
                    <Route exact path="/badges" component={Badges} />
                    <Route exact path="/badges/new" component={BadgeNew} />
                    <Route exact path="/badges/:badgeId/edit" component={BadgeEdit} />
                    <Route exact path="/badges/:badgeId" component={BadgeDetailsContainer} />
                    <Route component={NotFound} />
                </Switch>
            </Layout>
        </BrowserRouter>
    );
}

export default App;