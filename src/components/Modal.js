import React from "react";
import ReadtDOM from "react-dom";
import "./styles/Modal.css";

function Modal(props) {

    if (!props.isOpen) return null;

    return(
        ReadtDOM.createPortal(
            <div className="Modal">
                <div className="Modal__container">
                    <button className="Modal__close-button" onClick={props.onClose}>
                        x
                    </button>

                    {props.children}
                </div>
            </div>,
            document.getElementById('modal')
        )
    );
}

export default Modal;